public with sharing class ExpensesController {
    
    @AuraEnabled
    // All AuraEnabled controller methods must be STATIC methods + have scope of PUBLIC or GLOBAL
    public static List<Expense__c> getExpenses() {
        
        // Check that fields are accessible to user - with sharing is insufficient
        // Ideally, look into optimizing and caching access checks for better performance
        String[] fieldsToCheck = new String[]{
            'Id', 'Name', 'Amount__c', 'Client__c', 'Date__c',
            'Reimbursed__c', 'CreatedDate'
        };

        Map<String, Schema.SObjectField> fieldDescribeTokens = 
            Schema.SObjectType.Expense__c.fields.getMap();

        for(String field : fieldsToCheck){
            if(!fieldDescribeTokens.get(field).getDescribe().isAccessible()){
                throw new System.NoAccessException();
            }
        }
        // END field accessibility check

        return [SELECT Id, Name, Amount__c, Client__c, Date__c, Reimbursed__c, CreatedDate
                FROM Expense__c];
    }

    @AuraEnabled
    // variable from helper must match parameter in this method
    public static Expense__c saveExpense(Expense__c expense){
        
        // Check that fields are accessible to user - with sharing is insufficient
        // Ideally, look into optimizing and caching access checks for better performance
        String[] fieldsToCheck = new String[]{
            'Id', 'Name', 'Amount__c', 'Client__c', 'Date__c',
            'Reimbursed__c', 'CreatedDate'
        };

        Map<String, Schema.SObjectField> fieldDescribeTokens = 
            Schema.SObjectType.Expense__c.fields.getMap();

        for(String field : fieldsToCheck){
            if(!fieldDescribeTokens.get(field).getDescribe().isAccessible()){
                throw new System.NoAccessException();
            }
        }
        // END field accessibility check

        upsert expense;
        return expense;
    }
}
