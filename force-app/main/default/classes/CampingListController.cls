public with sharing class CampingListController {
    
    @AuraEnabled
    public static List<Camping_Item__c> getItems(){

        // Check that fields are accessible to user - with sharing is insufficient
        // Ideally, look into optimizing and caching access checks for better performance
        String[] fieldsToCheck = new String[]{
            'Id', 'CreatedDate', 'Name', 'Packed__c', 'Price__c', 'Quantity__c'
        };

        Map<String, Schema.SObjectField> fieldDescribeTokens = 
            Schema.SObjectType.Camping_Item__c.fields.getMap();

        for(String field : fieldsToCheck){
            if(!fieldDescribeTokens.get(field).getDescribe().isAccessible()){
                throw new System.NoAccessException();
            }
        }
        // END field accessibility check

        return [SELECT Id, CreatedDate, Name, Packed__c, Price__c, Quantity__c
                FROM Camping_Item__c];
    }

    @AuraEnabled
    public static Camping_Item__c saveItem(Camping_Item__c item){
        String[] fieldsToCheck = new String[]{
            'Id', 'CreatedDate', 'Name', 'Packed__c', 'Price__c', 'Quantity__c'
        };

        Map<String, Schema.SObjectField> fieldDescribeTokens = 
            Schema.SObjectType.Camping_Item__c.fields.getMap();

        for(String field : fieldsToCheck){
            if(!fieldDescribeTokens.get(field).getDescribe().isAccessible()){
                throw new System.NoAccessException();
            }
        }

        upsert item;
        return item;
    }
}
