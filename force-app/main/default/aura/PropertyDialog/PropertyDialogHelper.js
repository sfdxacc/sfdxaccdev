({
    navigateTo: function(component, recId) {

        // navigate to record after saveRecord runs
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recId
        });
        navEvt.fire();
    }
})
