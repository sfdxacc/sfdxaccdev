({
    // create an addItem event and fire event
    createItem : function(component, newItem) {

        //create addItem event with item to be added
        // matches aura:registerEvent tag in the component
        
        var createEvent = component.getEvent("addItem");
        createEvent.setParams({ "item": newItem });
        createEvent.fire();

        // reset to newItem value provider to blank sObjectType 
        // of Camping_Item__c
        component.set("v.newItem",
                      { 'sobjectType': 'Camping_Item__c',
                        'Name': '',
                        'Packed__c': false,
                        'Price__c': 0,
                        'Quantity__c': 0});
    }
})
