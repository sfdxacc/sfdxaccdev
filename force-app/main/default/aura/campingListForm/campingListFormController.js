({
    clickCreateItem : function(component, event, helper) {
        var validItem = component.find("campingItemForm").reduce(function(validSoFar, inputCmp){
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);

        // if the form is valid, call helper's createItem method
        if(validItem){
            var newItem = component.get("v.newItem");
            console.log("Create Camping Item: " + JSON.stringify(newItem));
            helper.createItem(component, newItem);
        }
    },
})
