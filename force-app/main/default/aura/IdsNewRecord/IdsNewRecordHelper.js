({
    validateContactForm : function(component) {
        var validContact = true;

        // Show error messages if required fields are blank
        var allValid = component.find('contactField').reduce(function (validFields, inputCmp){
            inputCmp.showHelpMessageIfInvalid();
            return validFields && inputCmp.get("v.validity").valid;
        }, true);

        if(allValid){
            // verify we have an account to attach it to
            var account = component.get("v.newContact");

            // if there is no Record
            if($A.util.isEmpty(account)){
                validContact = false;
                console.log("Quick action context doesn't have a valid account.");
            }
            return(validContact);
        }
    }
})
