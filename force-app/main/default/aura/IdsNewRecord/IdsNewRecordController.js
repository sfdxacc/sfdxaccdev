({
    doInit: function(component, event, helper) {
        // find where the recordData attribute is
        component.find("contactRecordCreator").getNewRecord(
            "Contact", // sObjectType
            null, // recordTypeId
            false, // skip cache?

            $A.getCallback(function(){
                // these are referring to the targetRecord and targetError in recordData
                var rec = component.get("v.newContact");
                var error = component.get("v.newContactError");

                if(error || (rec === null)){
                    console.log("Error initializing record template: " + error);
                }
                else{
                    console.log("Record template initialized: " + rec.apiName);
                }
            })
        )
    },

    handleSaveContact: function(component, event, helper){
        if(helper.validateContactForm(component)){

            // set the account record Id on the Contact AccountId field
            component.set("v.simpleNewContact.AccountId", component.get("v.recordId"));
            
            component.find("contactRecordCreator").saveRecord(function(saveResult){
                if(saveResult.state === "SUCCESS" || saveResult.state === "DRAFT"){
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Saved",
                        "message": "The record was saved."
                    });

                    resultsToast.fire();
                } else if (saveResult.state === "INCOMPLETE"){
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR"){
                    console.log("Problem saving contact, error: " + JSON.stringify(saveResult.error));
                } else {
                    console.log("Unknown problem, state: " + saveResult.state 
                                + ", error: " + JSON.stringify(saveResult.error));
                }
            })
        }
    }
})
