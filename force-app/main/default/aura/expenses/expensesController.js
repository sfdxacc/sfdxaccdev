({
    doInit : function(component, event, help){
        // Create action - it knows to call ExpensesController.getExpenses()
        var action = component.get("c.getExpenses");

        // Add callback behavior when we get a response
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(state == "SUCCESS"){
                component.set("v.expenses", response.getReturnValue());
            }

            else {
                console.log("Failed with state: " + state);
            }
        });
        // Send action off to be execute
        $A.enqueueAction(action);
    },

    handleCreateExpense: function(component, event, helper) {
        // expense refers to the expensesItemUpdate event name
        var newExpense = event.getParam("expense");
        helper.createExpense(component, newExpense);
    },
    
    handleUpdateExpense: function(component, event, helper){
        // expense refers to the expensesItemUpdate event name
        var updatedExp = event.getParam("expense");
        helper.updateExpense(component, updatedExp);
    }
})
