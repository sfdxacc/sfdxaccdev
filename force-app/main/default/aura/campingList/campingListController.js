({  
    doInit : function(component, event, helper){
        var action = component.get("c.getItems");

        // Add callback behavior when we get a response
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if(component.isValid() && state === "SUCCESS"){
                component.set("v.items", response.getReturnValue());
            }

            else {
                console.log("Failed with state: " + state);
            }
        });
        // Send action off to be execute
        $A.enqueueAction(action);
    },

    handleAddItem: function(component, event, helper) {
        // item refers to the addItemEvent name
        var newItem = event.getParam("item");
        
        //Trailhead doesn't like the helper...
        //helper.saveItem(component, newItem);
        
        var action = component.get("c.saveItem");
        action.setParams({"item": item});

        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
                var items = component.get("v.items");
                items.push(item);
                component.set("v.items", items);
            }
        });
        $A.enqueueAction(action);
    },
})
